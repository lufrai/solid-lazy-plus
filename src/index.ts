import { Component, createComponent } from "solid-js";
import { getRequestEvent, isServer } from "solid-js/web";
import { useHead } from "@solidjs/meta";
import { lazy as solidLazy } from "solid-js";
import type { Manifest } from "vite";

// TODO: implement these as public APIs in Vinxi?
const getApp = () => (globalThis as any).app;
const getRouter = (name: string) => getApp().getRouter(name);
const getBundlerManifest = (name: string): Manifest =>
  getApp().config.buildManifest[name];
const getBaseUrl = (name: string) => {
  const app = getApp();
  const router = getRouter(name);
  return app.config.server.baseURL ?? "" + router.base;
};
const cssById: Record<string, string[]> = {};
let clientManifest: Manifest;
let ssrManifest: Manifest;

const getModuleManifest = (id: string, chunkId?: string) => {
  clientManifest = clientManifest || getBundlerManifest("client");
  ssrManifest = ssrManifest || getBundlerManifest("ssr");

  let keys = [id];
  if (chunkId) keys.push(chunkId);

  for (const manifest of [ssrManifest, clientManifest]) {
    for (const key of keys) {
      const chunk = manifest[key];
      if (!chunk) {
        continue;
      }

      return {
        key,
        router: manifest === ssrManifest ? "ssr" : "client",
        manifest,
        chunk,
      };
    }
  }
};

// TODO: implement this in @solidjs/start?
const collectAssets = function <
  T extends () => Promise<{ default: Component<any> }>,
>(fn: T): T {
  // Vite handles lazy assets already properly during dev
  if (import.meta.env.DEV) {
    return fn;
  }

  const wrapper: any = async () => {
    const mod = await fn();
    let css: string[] = [];

    const wrappedMod = {
      default: (props: any) => {
        if (isServer) {
          const req = getRequestEvent() as any;
          const rendered: string[] = (req.locals.lazyPlusLinks =
            req.locals.lazyPlusLinks || []);
          for (const href of css) {
            if (rendered.includes(href)) {
              continue;
            }
            rendered.push(href);
            useHead({
              // TODO: what to use as id?
              id: "",
              tag: "link",
              props: {
                rel: "stylesheet",
                href,
              },
            });
          }
        }

        return createComponent(mod.default, props);
      },
    };

    if (!isServer) {
      return wrappedMod;
    }

    const id = (mod as any).id$$;
    const chunkId = (mod as any).ch$$;
    if (!id) return wrappedMod;

    if (cssById[id]) {
      css = cssById[id];
      return wrappedMod;
    }

    const result = getModuleManifest(id, chunkId);
    if (!result) return wrappedMod;
    const { manifest, key } = result;

    const base = getBaseUrl("client") + "/";

    const traversed: string[] = [];
    const traverse = function (id: string) {
      if (traversed.includes(id)) return;
      traversed.push(id);
      const chunk = manifest[id];
      if (!chunk) return;
      for (const url of chunk.css || []) {
        css.push(base + url);
      }
      for (const id of chunk.imports || []) {
        traverse(id);
      }
    };
    traverse(key);

    cssById[id] = css;
    return wrappedMod;
  };

  return wrapper;
};

export const lazy = <T extends Component<any>>(
  fn: () => Promise<{ default: T }>,
) => solidLazy(collectAssets(fn));

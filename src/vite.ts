import MagicString from "magic-string";
import { sep as osSep } from "path";
import { basename, relative, sep } from "path/posix";
import type { Plugin } from "vite";

const lazyAssetsPlugin = ({ router }: { router: string }): any => {
  if (router !== "server") return;
  const cwd = process.cwd().replaceAll(osSep, sep);
  return {
    name: "lazy-assets",
    enforce: "post",
    resolveImportMeta(prop, { chunkId }) {
      if (prop === "chunkId") {
        return `"_${basename(chunkId)}"`;
      }
    },
    transform(src, id) {
      if (!id.match(/(ts|js)x(\?.*)?$/)) return;
      if (src.indexOf("import") === -1) {
        return;
      }

      const localId = relative(cwd, id);

      const s = new MagicString(src);
      s.append(`export const id$$ = "${localId}";
export const ch$$ = import.meta.chunkId;
`);

      const code = s.toString();
      const map = s.generateMap();
      return { code, map };
    },
  } satisfies Plugin;
};

export default lazyAssetsPlugin;

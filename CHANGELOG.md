# solid-lazy-plus

## 0.4.1

### Patch Changes

- be31ce3: The module ids used for asset detection, are now properly normalized to relative paths on Windows. Thanks to [Echab](https://codeberg.org/echab) who brought up this issue and provided detailed information about the cause. (#1)

## 0.4.0

### Minor Changes

- 3b1168c: Reworked the asset resolution to support non-entry chunk keys.

## 0.3.0

### Minor Changes

- 963d5e7: Updated the dependencies.

## 0.2.0

### Minor Changes

- 39a72e0: Implemented deduplication and caching for the crawled lazy css.

### Patch Changes

- 3012aa8: `lazy` now always returns a wrapped component. This fixes some inconsistencies between client and server rendering.

## 0.1.2

### Patch Changes

- e1d7842: Fixed a bug with the `lazy` runtime breaking server-side rendering and client-side hydration of nested route layouts.

## 0.1.1

### Patch Changes

- 6585995: The transform now also applies to file paths ending with a query string. For example SolidStart route files end with `?pick=default&pick=$css`.

## 0.1.0

### Minor Changes

- f6fddc2: Changed the internal way how dynamically loaded module ids are tracked.

  Instead of exporting the id in all modules, it will now be appended to the actual dynamic import statements, and only in jsx/tsx modules. E.g. an import like `import("./lazyForm")` will be transformed to `import("./lazyForm").then(m => ({ ...m, id$: "[id]" }))`. This should result in a smaller build output and faster build times.

### Patch Changes

- 50520c5: `lazy` will now just return the original module and skip assets registration, if the module cannot be found in the prod manifest.

## 0.0.6

### Patch Changes

- 2ef64ed: Added repository url to pkg.

## 0.0.5

### Patch Changes

- 25c1dd7: Filled the README with actual content.

## 0.0.4

### Patch Changes

- e5a6f48: Added the `exports` field to package.json.

## 0.0.3

### Patch Changes

- 3254593: Opened up the peerDependency ranges to reduce duplicates.

## 0.0.2

### Patch Changes

- db6aa6a: Made sure that all files are included in the npm release.

## 0.0.1

### Patch Changes

- 931fafe: Implemented the first version.
